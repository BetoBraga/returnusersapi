<?php

$finder = PhpCsFixer\Finder::create()
  ->notPath('vendor')
  ->notPath('bootstrap')
  ->notPath('storage')
  ->in(__DIR__)
  ->name('*.php')
  ->notName('*.blade.php');

$config = new PhpCsFixer\Config();

return $config
  ->setRules([
    '@Symfony' => true,
    'array_syntax' => ['syntax' => 'short'],
    'ordered_imports' => ['sort_algorithm' => 'alpha'],
    'no_unused_imports' => true,
    'braces' => ['position_after_functions_and_oop_constructs' => 'same'],
    'curly_braces_position' => [
      'classes_opening_brace' => 'same_line',
      'functions_opening_brace' => 'same_line',
    ],
  ])
  ->setIndent('  ')
  ->setFinder($finder);
