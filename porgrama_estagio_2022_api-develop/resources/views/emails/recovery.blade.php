@extends('layouts.mail')

@section('content')
  <h1>Recuperação de Senha</h1>
  <div>
    <p>Uma recuperação de senha foi solicitada para o seu e-mail <strong>({{ $email }})</strong>. Se não foi você ignore este e-mail.</p>
    <p>Para recuperar a sua senha e criar uma nova, acesse <a href="{{ env('APP_URL') }}/password/reset/{{ $token }}">Criar nova senha</a></p>
  </div>
  </body>
@endsection
