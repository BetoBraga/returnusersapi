<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <style>
    * {
      margin: 0;
      box-sizing: border-box;
      list-style: none;
      padding: 0;
    }

    body {
      background-color: #2c2c2c;
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      padding: 24px;
      display: flex;
      flex-direction: column;
      gap: 24px;
    }

    h1 {
      text-align: center;
      font-weight: 600;
      font-size: 24px;
    }

    div {
      gap: 12px;
      display: flex;
      flex-direction: column;
    }

    a {
      font-weight: 600;
      color: #4343ff;
    }
  </style>
</head>
<body>
  @yield('content')
</body>
</html>
