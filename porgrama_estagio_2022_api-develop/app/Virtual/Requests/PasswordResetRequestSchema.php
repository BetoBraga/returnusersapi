<?php

namespace App\Virtual\Requests;

/**
 * @OA\Schema(
 *   title="PasswordResetRequest",
 *   description="Password reset request",
 *   type="object",
 *   required={"token", "password", "password_confirmation"},
 *   @OA\Xml(
 *      name="PasswordResetRequestSchema"
 *   )
 * )
 */
class PasswordResetRequestSchema {
  /**
   * @OA\Property(
   *   title="password",
   *   description="User's new password",
   *   example="YourStr0ng(!)Passw0rd"
   * )
   *
   * @var string
   */
  public $password;

  /**
   * @OA\Property(
   *   title="password_confirmation",
   *   description="User's new password",
   *   example="YourStr0ng(!)Passw0rd"
   * )
   *
   * @var string
   */
  public $password_confirmation;
}
