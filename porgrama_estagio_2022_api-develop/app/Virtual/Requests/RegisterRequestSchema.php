<?php

namespace App\Virtual\Requests;

/**
 * @OA\Schema(
 *  title="RegisterRequest",
 *  description="User register request",
 *  type="object",
 *  required={"name", "email", "password"},
 *  @OA\Xml(
 *  name="RegisterRequestSchema")
 * )
 */
class RegisterRequestSchema {
  /**
   * @OA\Property(
   *  title="name",
   *  description="User name",
   *  example="João Gilberto",
   * )
   *
   * @var string
   */
  public $name;
  /**
   * @OA\Property(
   *  title="email",
   *  description="User email",
   *  example="example@email-provider.com",
   * )
   *
   * @var string
   */
  public $email;

  /**
   * @OA\Property(
   *  title="password",
   *  description="User password",
   *  example="YourStr0ng(!)Passw0rd",
   * )
   *
   * @var string
   */
  public $password;
}
