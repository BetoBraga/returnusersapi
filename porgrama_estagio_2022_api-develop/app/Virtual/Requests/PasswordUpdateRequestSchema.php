<?php

namespace App\Virtual\Requests;

/**
 * @OA\Schema(
 *   title="PasswordUpdateRequest",
 *   description="Password update request",
 *   type="object",
 *   required={"current_password", "password", "password_confirmation"},
 *   @OA\Xml(
 *      name="PasswordUpdateRequestSchema"
 *   )
 * )
 */
class PasswordUpdateRequestSchema {
  /**
   * @OA\Property(
   *   title="current_password",
   *   description="User's current password"
   * )
   *
   * @var string
   */
  public $current_password;

  /**
   * @OA\Property(
   *   title="password",
   *   description="User's new password",
   *   example="YourStr0ng(!)Passw0rd"
   * )
   *
   * @var string
   */
  public $password;

  /**
   * @OA\Property(
   *   title="password_confirmation",
   *   description="User's new password",
   *   example="YourStr0ng(!)Passw0rd"
   * )
   *
   * @var string
   */
  public $password_confirmation;
}
