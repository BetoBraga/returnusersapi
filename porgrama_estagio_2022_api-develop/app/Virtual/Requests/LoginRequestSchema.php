<?php

namespace App\Virtual\Requests;

/**
 * @OA\Schema(
 *  title="LoginRequest",
 *  description="Authentication login request",
 *  type="object",
 *  required={"email", "password"},
 *  @OA\Xml(
 *  name="LoginRequestSchema")
 * )
 */
class LoginRequestSchema {
  /**
   * @OA\Property(
   *  title="email",
   *  description="User email",
   *  example="example@email-provider.com"
   * )
   *
   * @var string
   */
  public $email;

  /**
   * @OA\Property(
   *  title="password",
   *  description="User password",
   *  example="YourStr0ng(!)Passw0rd"
   * )
   *
   *  @var string
   */
  public $password;
}
