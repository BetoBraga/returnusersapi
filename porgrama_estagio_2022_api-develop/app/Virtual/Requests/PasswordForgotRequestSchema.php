<?php

namespace App\Virtual\Requests;

/**
 * @OA\Schema(
 *   title="PasswordForgotRequest",
 *   description="Password forgot request",
 *   type="object",
 *   required={"email"},
 *   @OA\Xml(
 *      name="PasswordForgotRequestSchema"
 *   )
 * )
 */
class PasswordForgotRequestSchema {
  /**
   * @OA\Property(
   *   title="email",
   *   description="User's email",
   *   example="example@email-provider.com"
   * )
   *
   * @var string
   */
  public $email;
}
