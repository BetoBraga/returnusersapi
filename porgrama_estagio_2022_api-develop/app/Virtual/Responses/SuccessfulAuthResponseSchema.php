<?php

namespace App\Virtual\Responses;

/**
 * @OA\Schema(
 *   title="SuccessfulAuthResponse",
 *   description="Authentication response body",
 *   @OA\Xml(
 *      name="SuccessfulAuthResponseSchema"
 *   )
 * )
 */
class SuccessfulAuthResponseSchema {
  /**
   * @OA\Property(
   *   title="access_token",
   *   description="User JWT access token"
   * )
   *
   * @var string;
   */
  public $access_token;

  /**
   * @OA\Property(
   *   title="token_type",
   *   description="Token type"
   * )
   *
   * @var string
   */
  public $token_type;

  /**
   * @OA\Property(
   *   title="expires_in",
   *   description="Token expiration time"
   * )
   *
   * @var int
   */
  public $expires_in;
}
