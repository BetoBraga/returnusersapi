<?php

namespace App\Virtual\Responses;

/**
 * @OA\Schema(
 *   title="SuccessfulOperationResponse",
 *   @OA\Xml(
 *      name="SuccessfulOperationResponseSchema"
 *   )
 * )
 */
class SuccessfulOperationResponseSchema {
  /**
   * @OA\Property(
   *   title="message"
   * )
   *
   * @var string;
   */
  public $message;
}
