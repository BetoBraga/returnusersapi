<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordForgotRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\PasswordUpdateRequest;
use App\Mail\PasswordRecoveryMail;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PasswordController extends Controller {
  public function __construct() {
    $this->middleware('auth:api', [
      'except' => [
        'forgotPassword',
        'resetPassword',
      ],
    ]);
  }

  /**
   * @OA\Put(
   *   path="/api/password/update",
   *   operationId="updatePassword",
   *   tags={"User"},
   *   summary="Update a user's password",
   *   security={ {"sanctum": {} }},
   *   description="Returns the successful operation",
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/PasswordUpdateRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Password successfully changed",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulOperationResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Update a user's password.
   *
   * @return JsonResponse
   */
  public function updatePassword(PasswordUpdateRequest $request) {
    $this->middleware('auth:api');

    $data = $request->validated();
    $user = auth()->user();

    $currentPassword = $data['current_password'];
    $newPassword = $data['password'];
    $user->password = Hash::make($newPassword);
    $user->save();

    return response()->json([
      'message' => 'Password successfully changed',
    ]);
  }

  /**
   * @OA\Post(
   *   path="/api/password/forgot",
   *   operationId="forgotPassword",
   *   tags={"User"},
   *   summary="Recover a user's password",
   *   description="Returns the successful operation",
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/PasswordForgotRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Email successfully sent",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulOperationResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=400,
   *      description="Cannot send the email"
   *   ),
   *   @OA\Response(
   *      response=404,
   *      description="User not found"
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Recover a user's password.
   *
   * @return JsonResponse
   */
  public function forgotPassword(PasswordForgotRequest $request) {
    $data = $request->validated();
    $user = User::findByEmail($data['email']);

    if (!$user) {
      return response()->json([
        'message' => 'User not found',
      ], 404);
    }

    $email = $user['email'];
    $token = Str::random(60);
    $create_at = Carbon::now();

    DB::table('password_resets')->where([
      'email' => $email,
    ])->delete();

    DB::table('password_resets')->insert([
      'email' => $email,
      'token' => $token,
      'created_at' => $create_at,
    ]);

    try {
      Mail::to($email)->send(new PasswordRecoveryMail($email, $token));

      return response()->json([
        'message' => 'Email successfully sent',
      ], 200);
    } catch (\Throwable $th) {
      return response()->json([
        'message' => 'Cannot send the email',
      ], 400);
    }
  }

  /**
   * @OA\Post(
   *   path="/api/password/reset",
   *   operationId="resetPassword",
   *   tags={"User"},
   *   summary="Reset a user's password",
   *   description="Returns the successful operation",
   *   @OA\Parameter(
   *    name="token",
   *    in="path",
   *    description="Token",
   *    required=true,
   *   ),
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/PasswordResetRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Password successfully change",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulOperationResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=401,
   *      description="Expired token time"
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Reset a user's password.
   *
   * @return JsonResponse
   */
  public function resetPassword(PasswordResetRequest $request, string $token) {
    $data = $request->validated();

    $resetPassword = DB::table('password_resets')
      ->where([
        'token' => $token,
      ])->first();

    $token_created_at = Carbon::createFromTimestamp($resetPassword->created_at);

    if ($token_created_at->addHour() > now()) {
      DB::table('password_resets')->where([
        'token' => $token,
      ])->delete();

      return response()->json([
        'message' => 'Expired token time',
      ]);
    }

    $user = User::where('email', $resetPassword->email)
      ->update(['password' => Hash::make($data['password'])]);

    DB::table('password_resets')->where([
      'token' => $token,
    ])->delete();

    return response()->json([
      'message' => 'Password successfully changed',
    ]);
  }
}
