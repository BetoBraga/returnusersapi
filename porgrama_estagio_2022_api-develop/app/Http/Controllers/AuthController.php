<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {
  public function __construct() {
    $this->middleware('auth:api', [
      'except' => ['login', 'register'],
    ]);
  }

  /**
   * @OA\Post(
   *   path="/api/auth/login",
   *   operationId="login",
   *   tags={"Auth"},
   *   summary="Authenticate an user",
   *   description="Returns the access token",
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/LoginRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Successfully logged",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulAuthResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Authenticate an user
   *
   * @return JsonResponse
   */
  public function login(LoginRequest $request) {
    $credentials = $request->validated();
    $token = auth()->attempt($credentials);

    if (!$token) {
      return response()->json(['error' => 'Unauthorized'], 422);
    }

    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => auth()->factory()->getTTL() * 60,
    ], 422);
  }

  /**
   * @OA\Post(
   *   path="/api/auth/register",
   *   operationId="register",
   *   tags={"Auth"},
   *   summary="Register and authenticate an user",
   *   description="Returns the access token",
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/RegisterRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Successfully logged",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulAuthResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Registers and authenticates a user.
   *
   * @return JsonResponse
   */
  public function register(RegisterRequest $request) {
    $credentials = $request->validated();

    $user = User::create([
      'name' => $credentials['name'],
      'email' => $credentials['email'],
      'password' => Hash::make($credentials['password']),
    ]);

    $token = auth()->login($user);

    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => auth()->factory()->getTTL() * 60,
    ], 201);
  }

  /**
   * @OA\Post(
   *   path="/api/auth/logout",
   *   operationId="logout",
   *   tags={"Auth"},
   *   security={ {"sanctum": {} }},
   *   summary="Log an user out",
   *   description="Log an user out and returns a message",
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/LoginRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Successfully signed out",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulOperationResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Log an user out.
   *
   * @return JsonResponse
   */
  public function logout() {
    auth()->logout();

    return response()->json([
      'message' => 'Successfully signed out',
    ]);
  }

  /**
   * @OA\Post(
   *   path="/api/auth/refresh",
   *   operationId="refresh",
   *   tags={"Auth"},
   *   security={ {"sanctum": {} }},
   *   summary="Refresh the access token",
   *   description="Returns the access token refreshed",
   *   @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(ref="#/components/schemas/LoginRequestSchema")
   *   ),
   *   @OA\Response(
   *      response=200,
   *      description="Successfully refreshed",
   *      @OA\JsonContent(ref="#/components/schemas/SuccessfulAuthResponseSchema")
   *   ),
   *   @OA\Response(
   *      response=422,
   *      description="Unprocessable content"
   *   )
   * )
   *
   * Log an user out.
   *
   * @return JsonResponse
   */
  public function refresh() {
    return response()->json([
      'access_token' => auth()->refresh(),
      'token_type' => 'bearer',
      'expires_in' => auth()->factory()->getTTL() * 60,
    ]);
  }
}
