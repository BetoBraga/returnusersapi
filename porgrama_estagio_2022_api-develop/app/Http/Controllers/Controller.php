<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *   title="Estágio API",
 *   description="API developed during the IT internship",
 *   version="1.0.0"
 * )
 * @OA\Server(
 *   url=L5_SWAGGER_CONST_HOST,
 *   description="Demo API"
 * )
 * @OA\Tag(
 *   name="Auth",
 *   description="Endpoints for authentication"
 * )
 * @OA\Tag(
 *   name="User",
 *   description="Endpoints for users"
 * )
 */
class Controller extends BaseController {
  use AuthorizesRequests;
  use DispatchesJobs;
  use ValidatesRequests;
}
