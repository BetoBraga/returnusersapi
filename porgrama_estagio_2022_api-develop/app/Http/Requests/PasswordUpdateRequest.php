<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PasswordUpdateRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
        'current_password' => 'required|string|min:8|current_password:api',
        'password' => 'required|string|min:8',
        'password_confirmation' => 'same:password',
    ];
  }

  public function messages() {
    return [
      'current_password.required' => 'The current_password field is required',
      'current_password.string' => 'The current_password must be a string',
      'current_password.min' => 'The current_password must be greater than :min',
      'current_password.current_password' => 'The passwords does not match',

      'password.required' => 'The password field is required',
      'password.string' => 'The password must be a string',
      'password.min' => 'The password must be greater than :min',

      'password_confirmation.same' => 'The passwords must be the same',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
  }
}
