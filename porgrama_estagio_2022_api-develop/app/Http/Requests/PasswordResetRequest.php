<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PasswordResetRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'password' => 'required|string|min:8',
      'password_confirmation' => 'same:password',
    ];
  }

  public function messages() {
    return [
      'password.required' => 'The password field is required',
      'password.string' => 'The password must be a string',
      'password.min' => 'The password must be greater than :min',

      'password_confirmation.same' => 'The passwords must be the same',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
  }
}
