<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:8',
    ];
  }

  public function messages() {
    return [
      'name.required' => 'The name field is required',
      'name.string' => 'The name must be a string',
      'name.max' => 'The name must be less than :max',

      'email.required' => 'The email field is required',
      'email.string' => 'The email must be a string',
      'email.email' => 'The email field must be a valid email address',
      'email.max' => 'The email must be less than :max',
      'email.unique' => 'The provided email is already in use',

      'password.required' => 'The password field is required',
      'password.string' => 'The password field must be a string',
      'password.min' => 'The password must be greater than :min',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
  }
}
