<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordRecoveryMail extends Mailable {
  use Queueable;
  use SerializesModels;

  protected $email;
  protected $token;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($email, $token) {
    $this->email = $email;
    $this->token = $token;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build() {
    return $this->view('emails.recovery', [
      'email' => $this->email,
      'token' => $this->token,
    ]);
  }
}
