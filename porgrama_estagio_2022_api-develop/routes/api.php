<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PasswordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//   return $request->user();
// });

Route::controller(AuthController::class)
  ->prefix('auth')
  ->group(function () {
    Route::post('login', 'login')
      ->name('auth.login');
    Route::post('register', 'register')
      ->name('auth.register');
    Route::get('me', 'me')
      ->name('auth.me');
    Route::post('logout', 'logout')
      ->name('auth.logout');
    Route::post('refresh', 'refresh')
      ->name('auth.refresh');
  });

Route::controller(PasswordController::class)
  ->prefix('password')
  ->group(function () {
    Route::put('update', 'updatePassword')
      ->name('password.updatePassword');
    Route::post('forgot', 'forgotPassword')
      ->name('password.forgotPassword');
    Route::post('reset/{token}', 'resetPassword')
      ->name('password.resetPassword');
  });
