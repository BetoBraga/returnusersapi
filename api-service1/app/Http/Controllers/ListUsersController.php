<?php

namespace App\Http\Controllers;

use App\Models\User;

class ListUsersController extends Controller
{
    public function listUsers()
    {
        return response()->json(User::all());
    }
}
